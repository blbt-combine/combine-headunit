/***************************************************************************//**
 * @file hal_efr.c
 * @brief This file contains EFR32 specific HAL code to handle chip startup.
 * @copyright Copyright 2015 Silicon Laboratories, Inc. http://www.silabs.com
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>

#include "em_device.h"
#include "em_cmu.h"
#include "em_emu.h"
//#include "bsp.h"
#include "em_chip.h"
#include "pti.h"
#include "pa.h"

#include "rail_config.h"
#include "hal_common.h"


#if !defined(RADIO_PA_2P4_INIT)
#define RADIO_PA_2P4_INIT                                                     \
  {                                                                           \
    PA_SEL_2P4_HP,    /* Power Amplifier mode */                              \
    PA_VOLTMODE_DCDC, /* Power Amplifier vPA Voltage mode */                  \
    100,              /* Desired output power in dBm * 10 */                  \
    0,                /* Output power offset in dBm * 10 */                   \
    10,               /* Desired ramp time in us */                           \
  }
#endif



/* modified by Sebastian: just doing the PA init. */
void halInitChipSpecific(void)
{
  RADIO_PAInit_t paInit;
  paInit = (RADIO_PAInit_t) RADIO_PA_2P4_INIT;


  if (!RADIO_PA_Init(&paInit)) {
    // Error: The PA could not be initialized due to an improper configuration.
    // Please ensure your configuration is valid for the selected part.
    while(1);
  }

  // Disable any unusd peripherals to ensure we enter a low power mode
  //boardLowPowerInit();
}

// Simple microsecond delay routine that has been calibrated for an EFR32
// running at 38.4MHz. This code should be moved to a more clean location
// along with all board initialization in the future.
static const uint32_t iterations_per_microsecond = 3;
static void usecDelay(uint32_t usecs)
{
  volatile uint64_t iterations = iterations_per_microsecond * usecs;

  while(iterations--);
}

// Create defines for the different PRS signal sources as they vary per chip
#if _SILICON_LABS_32B_SERIES_1_CONFIG == 1
// Defines for EFR32xG1 chips
#define _PRS_CH_CTRL_SOURCESEL_RAC     0x00000020UL
#define _PRS_CH_CTRL_SOURCESEL_FRC     0x00000025UL
#define _PRS_CH_CTRL_SOURCESEL_MODEML  0x00000026UL
#else
// Defines for EFR32xG12 and newer chips
#define _PRS_CH_CTRL_SOURCESEL_RAC     0x00000051UL
#define _PRS_CH_CTRL_SOURCESEL_FRC     0x00000055UL
#define _PRS_CH_CTRL_SOURCESEL_MODEML  0x00000056UL
#endif

/**
 * Define the signals that are supported for debug in railtest. These are chip
 * specific because on some chips these are supported by the PRS while on others
 * the debugging must come from the library directly.
 */
static const debugSignal_t debugSignals[] =
{
  {
    .name = "RXACTIVE",
    .isPrs = true,
    .loc = {
      .prs = {
        .signal = 0x02,
        .source = _PRS_CH_CTRL_SOURCESEL_RAC
      }
    }
  },
  {
    .name = "TXACTIVE",
    .isPrs = true,
    .loc = {
      .prs = {
        .signal = 0x01,
        .source = _PRS_CH_CTRL_SOURCESEL_RAC
      }
    }
  },
  {
    .name = "LNAEN",
    .isPrs = true,
    .loc = {
      .prs = {
        .signal = 0x03,
        .source = _PRS_CH_CTRL_SOURCESEL_RAC
      }
    }
  },
  {
    .name = "PAEN",
    .isPrs = true,
    .loc = {
      .prs = {
        .signal = 0x04,
        .source = _PRS_CH_CTRL_SOURCESEL_RAC
      }
    }
  },
  {
    .name = "PTIDATA",
    .isPrs = true,
    .loc = {
      .prs = {
        .signal = 0x00,
        .source = _PRS_CH_CTRL_SOURCESEL_FRC
      }
    }
  },
};

const debugSignal_t* halGetDebugSignals(uint32_t *size)
{
  if (size != NULL) {
    *size = sizeof(debugSignals)/sizeof(debugSignal_t);
  }
  return debugSignals;
}

/**
 * Define the pins that are supported for debugging on the EFR32. This includes
 * PF2, PF3, PF4, PF5, PC10, and PC11. Along with these pins there are specific
 * PRS channels that will be used to output debug information on these pins.
 * This is allo for debug and very specific to the EFR32.
 */
static const debugPin_t debugPins[] = {
  {
    .name = "PC10",
    .prsChannel = 9,
    .prsLocation = 15,
    .gpioPort = gpioPortC,
    .gpioPin = 10
  },
  {
    .name = "PC11",
    .prsChannel = 10,
    .prsLocation = 5,
    .gpioPort = gpioPortC,
    .gpioPin = 11
  },
  {
    .name = "PF2",
    .prsChannel = 0,
    .prsLocation = 2,
    .gpioPort = gpioPortF,
    .gpioPin = 2
  },
  {
    .name = "PF3",
    .prsChannel = 1,
    .prsLocation = 2,
    .gpioPort = gpioPortF,
    .gpioPin = 3
  },
  {
    .name = "PF4",
    .prsChannel = 2,
    .prsLocation = 2,
    .gpioPort = gpioPortF,
    .gpioPin = 4
  },
  {
    .name = "PF5",
    .prsChannel = 3,
    .prsLocation = 2,
    .gpioPort = gpioPortF,
    .gpioPin = 5
  },
};

const debugPin_t* halGetDebugPins(uint32_t *size)
{
  if (size != NULL) {
    *size = sizeof(debugPins)/sizeof(debugPin_t);
  }
  return debugPins;
}

void halDisablePrs(uint8_t channel)
{
  // Turn the specified PRS channel off
  BUS_RegBitWrite(&PRS->ROUTEPEN,
                  _PRS_ROUTEPEN_CH0PEN_SHIFT + channel,
                  0);
}

void halEnablePrs(uint8_t channel, uint8_t loc, uint8_t source, uint8_t signal)
{
  volatile uint32_t *routeLocPtr;

  // Make sure the PRS is on and clocked
  CMU_ClockEnable(cmuClock_PRS, true);

  // Make sure this PRS channel is off while reconfiguring
  BUS_RegBitWrite(&PRS->ROUTEPEN,
                  _PRS_ROUTEPEN_CH0PEN_SHIFT + channel,
                  0);

  PRS->CH[channel].CTRL = signal << _PRS_CH_CTRL_SIGSEL_SHIFT
                                 | source << _PRS_CH_CTRL_SOURCESEL_SHIFT
                                 | PRS_CH_CTRL_EDSEL_OFF;

  // Configure the output location for this PRS channel
  routeLocPtr   = &PRS->ROUTELOC0 + (channel/4);
  *routeLocPtr &= ~(0xFF << (_PRS_ROUTELOC0_CH1LOC_SHIFT
                          * (channel % 4)));
  *routeLocPtr |= loc << (_PRS_ROUTELOC0_CH1LOC_SHIFT
                          * (channel % 4));

  // Set the enable bit for this PRS channel
  BUS_RegBitWrite(&PRS->ROUTEPEN,
                  _PRS_ROUTEPEN_CH0PEN_SHIFT + channel,
                  1);
}
