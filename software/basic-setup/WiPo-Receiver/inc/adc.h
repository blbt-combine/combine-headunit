/*******************************************************************************
 * ADC LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 29.06.2017
 * Description: Handling basic functions of the ADC.
 *
 ******************************************************************************/

#ifndef INC_ADC_H_
#define INC_ADC_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "config.h"
#include "em_device.h"
#include "em_adc.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

#define ADC_INITSINGLE_WIPOREADER                                                 \
{                                                                                 \
  adcPRSSELCh0,              /* PRS ch0 (if enabled). */                          \
  adcAcqTime4,               /* 4 ADC_CLK cycle acquisition time. */              \
  adcRef2V5,        		 /* 2.5V internal reference. */                       \
  adcRes12Bit,               /* 12 bit resolution. */                             \
  adcPosSelTEMP,       		 /* Select temp. sensor as posSel */                  \
  adcNegSelVSS,              /* Select VSS as negSel */                           \
  false,                     /* Single ended input. */                            \
  false,                     /* PRS disabled. */                                  \
  false,                     /* Right adjust. */                                  \
  false,                     /* Deactivate conversion after one scan sequence. */ \
  false,                     /* No EM2 DMA wakeup from single FIFO DVL */         \
  false                      /* Discard new data on full FIFO. */                 \
}


/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void adcInit(void);
extern void adcSetChannel(uint8_t pin);
extern uint32_t adcRead(void);

#endif /* INC_SERIAL_H_ */
