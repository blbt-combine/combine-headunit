/*******************************************************************************
 * SERIAL LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 11.04.2017
 * Description: A basic library for sending strings over the serial
 * 		interface.
 *
 ******************************************************************************/

#ifndef INC_SERIAL_H_
#define INC_SERIAL_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "config.h"
#include "em_device.h"
#include "em_chip.h"
#include "em_usart.h"
#include <stdlib.h>

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

#define defaultUSART USART0;

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void serialInit(void);

extern uint8_t serialAvailable(void);
extern uint8_t serialCheckError(void);
extern void serialFlush(void);
extern int serialRead(void);

void serialWrite(uint8_t);
extern void serialPrintInt(uint32_t);
extern void serialPrint(char * data);
extern void serialPrintLn(char * data);

#endif /* INC_SERIAL_H_ */
