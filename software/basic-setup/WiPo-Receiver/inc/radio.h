/*******************************************************************************
 * RADIO LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 03.07.2017
 * Description: A simple library for accessing the RAIL library
 *
 ******************************************************************************/

#ifndef INC_RADIO_H_
#define INC_RADIO_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rail.h"
#include "rail_types.h"
#include "rail_config.h"

#include "em_device.h"
#include "em_gpio.h"

#include "InitDevice.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

// Memory manager configuration
#define MAX_BUFFER_SIZE  256

// General application memory sizes
#define APP_MAX_PACKET_LENGTH  (MAX_BUFFER_SIZE - 12) /* sizeof(RAIL_RxPacketInfo_t) == 12) */

#if !defined(RADIO_PA_2P4_INIT)
#define RADIO_PA_2P4_INIT                                                     \
  {                                                                           \
    PA_SEL_2P4_HP,    /* Power Amplifier mode */                              \
    PA_VOLTMODE_DCDC, /* Power Amplifier vPA Voltage mode */                  \
    100,              /* Desired output power in dBm * 10 */                  \
    0,                /* Output power offset in dBm * 10 */                   \
    10,               /* Desired ramp time in us */                           \
  }
#endif

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

extern volatile uint8_t txBuffer[APP_MAX_PACKET_LENGTH];
extern volatile uint16_t txBufferLength;

extern volatile uint8_t rxBuffer[APP_MAX_PACKET_LENGTH];
extern volatile uint16_t rxBufferLength;

extern volatile bool packetTx;
extern volatile bool packetRx;

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

void initRadio(void);
void radioTx(uint8_t *, uint16_t);
void radioStartRx(void);

#endif /* INC_RADIO_H_ */
