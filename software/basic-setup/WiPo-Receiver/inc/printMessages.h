/*
 * printMessages.h
 *
 *  Created on: 12.04.2017
 *      Author: stoec
 */

#ifndef INC_PRINTMESSAGES_H_
#define INC_PRINTMESSAGES_H_

#define FGPA_PROGRAMMING_SUCCESSFUL "FPGA has successfully being programmed."

#endif /* INC_PRINTMESSAGES_H_ */
