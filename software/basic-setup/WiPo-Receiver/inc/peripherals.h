/*******************************************************************************
 * PERIPHERALS LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 13.04.2017
 * Description: A basic library to encapsulate control of board peripherals.
 *
 ******************************************************************************/

#ifndef INC_PERIPHERAL_H_
#define INC_PERIPHERAL_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "em_device.h"
#include "em_gpio.h"
#include "em_vdac.h"
#include "em_cmu.h"
#include "InitDevice.h"
#include "gpiointerrupt.h"
#include "config.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void osc_enable(void);
extern void osc_disable(void);
extern void dac_enable(void);

#endif /* INC_PERIPHERAL_H_ */
