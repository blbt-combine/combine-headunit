/*******************************************************************************
 * GLOBAL CONFIG
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 28.06.2017
 * Description: A header file for global config
 *
 ******************************************************************************/

#ifndef INC_CONFIG_H_
#define INC_CONFIG_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

#define TICK_TIME_US		1000

#define DEBUG_PRINT			true // sets debug output by serial interface
#define DEBUG_PRINT_RAIL	false // sets debug output by serial interface

/* variables for selecting if configuration for corresponding module is set up
 * by the library (true) or by the hardware configurator (false) */

#define TIMER_LIB_INIT		true
#define SERIAL_LIB_INIT		true

/* variables for depicting ADC channel vs. pin */

#if BOARD_REVISION == 6
#define BAT_VOLTAGE			7
#define PA_VOLTAGE			48
#define PA_CURRENT			47
#define PA_TEMP				33
#define COIL_VOLTAGE		36
#define COIL_CURRENT		35
#endif


/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/


#endif /* INC_CONFIG_H_ */
