/*******************************************************************************
 * TIMER LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 27.06.2017
 * Description: Timer Lib
 *
 ******************************************************************************/

#ifndef INC_TIMER_H_
#define INC_TIMER_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "config.h"
#include "em_device.h"
#include "em_timer.h"
#include "InitDevice.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

extern volatile uint32_t tick;
void (*callback)(void);

/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void TIMER0_IRQHandler(void);
extern void timerInit(uint32_t, void *);
extern void wait_us(uint32_t);
extern void wait_ms(uint32_t);

#endif /* INC_TIMER_H_ */
