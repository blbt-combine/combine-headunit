/*******************************************************************************
 * FPGA FLASHER LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 11.04.2017
 * Description: A basic library to flash an iCE40 device using SPI.
 *      The library assumes that USART module as well
 *      as the I/Os have successfully been configured.
 *
 *****************************************************************************/

#ifndef INC_FPGA_FLASHER_H_
#define INC_FPGA_FLASHER_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "em_device.h"
#include "em_gpio.h"
#include "em_usart.h"

/* Include file for pin definitions: */
#include "InitDevice.h"

/******************************************************************************
 * LIBRARY DEFINITIONS
 *****************************************************************************/

#define defaultSPI		USART1


/******************************************************************************
 * LIBRARY FUNCTION PROTOTYPES
 *****************************************************************************/

extern void fpga_reset(void);
extern void fpga_flash(const unsigned char * binary, unsigned int binary_len);

#endif /* INC_FPGA-FLASHER_H_ */
