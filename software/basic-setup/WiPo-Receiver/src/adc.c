/*******************************************************************************
 * ADC LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 29.06.2017
 * Description: Handling basic functions of the ADC.
 *
 ******************************************************************************/

#include "adc.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

ADC_Init_TypeDef ADC0_init = ADC_INIT_DEFAULT;
ADC_InitSingle_TypeDef ADC0_init_single = ADC_INITSINGLE_WIPOREADER;

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

void adcInit(void) {

	ADC0_init.ovsRateSel = adcOvsRateSel4;
	ADC0_init.warmUpMode = adcWarmupKeepInStandby;
	ADC0_init.timebase = ADC_TimebaseCalc(0);
	ADC0_init.prescale = ADC_PrescaleCalc(8000000, 0);
	ADC0_init.tailgate = 0;
	ADC0_init.em2ClockConfig = adcEm2Disabled;

	ADC_Init(ADC0, &ADC0_init);
	// [ADC0_Init]$

	ADC_InitSingle(ADC0, &ADC0_init_single);
	// [ADC0_InputConfiguration]$
}

void adcSetChannel(uint8_t pin) {

	switch (pin) {
	case 7:
		ADC0_init_single.posSel = adcPosSelAPORT2YCH22;
		break; // Pin 7
	case 33:
		ADC0_init_single.posSel = adcPosSelAPORT3YCH29;
		break; // Pin 33
	case 35:
		ADC0_init_single.posSel = adcPosSelAPORT3XCH30;
		break; // Pin 35
	case 36:
		ADC0_init_single.posSel = adcPosSelAPORT3YCH31;
		break; // Pin 36
	case 47:
		ADC0_init_single.posSel = adcPosSelAPORT1XCH10;
		break; // Pin 47
	case 48:
		ADC0_init_single.posSel = adcPosSelAPORT1YCH11;
		break; // Pin 48
	default:
		ADC0_init_single.posSel = adcPosSelTEMP;
		break;	//Default: Temperature sensor
	}

	ADC_InitSingle(ADC0, &ADC0_init_single);
}

uint32_t adcRead(void) {
	ADC_Start(ADC0, adcStartSingle);
	while (( ADC0->STATUS & ADC_STATUS_SINGLEDV) == 0) {
	}
	return ADC_DataSingleGet(ADC0);
}
