/*******************************************************************************
 * PERIPHERALS LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 13.04.2017
 * Description: A basic library to encapsulate control of board peripherals.
 *
 ******************************************************************************/

#include "peripherals.h"
#include "misc.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

void osc_enable(void) {
	/* Set CLKOUT1 output to HFXOQ */
	CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_CLKOUTSEL1_MASK)
			| CMU_CTRL_CLKOUTSEL1_HFXOQ;

}

void osc_disable(void) {
	/* Disable CLKOUT1 output */
	CMU->CTRL = (CMU->CTRL & ~_CMU_CTRL_CLKOUTSEL1_MASK)
			| CMU_CTRL_CLKOUTSEL1_DISABLED;
}

void dac_enable(void) {
	// Enable clock:
	CMU_ClockEnable(cmuClock_VDAC0, true);

	VDAC_Init_TypeDef vdacInit = VDAC_INIT_DEFAULT;
	VDAC_InitChannel_TypeDef vdacCh0Init = VDAC_INITCHANNEL_DEFAULT;
	VDAC_InitChannel_TypeDef vdacCh1Init = VDAC_INITCHANNEL_DEFAULT;

	// Set prescaler to get 1 MHz VDAC clock frequency.
	vdacInit.prescaler = VDAC_PrescaleCalc(1000000, true, 0);
	vdacInit.reference = vdacRefAvdd;
	VDAC_Init(VDAC0, &vdacInit);

	// Enable channels:
	vdacCh0Init.enable = true;
	vdacCh1Init.enable = true;

	// Enable both channels:
	VDAC_InitChannel(VDAC0, &vdacCh0Init, 0);
	VDAC_InitChannel(VDAC0, &vdacCh1Init, 1);
}

