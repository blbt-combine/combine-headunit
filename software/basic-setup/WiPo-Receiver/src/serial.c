/*******************************************************************************
 * SERIAL LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 08.04.2017
 * Description: A basic library for sending strings over the serial
 * 		interface.
 * Example:
 *
 * 	char string[] = "Kapier ich nicht!";
 * 	serialPrintLn(string);
 *
 ******************************************************************************/

#include "serial.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

/* Receive ring buffer */
#define RXBUFSIZE	256					// Buffer size for RX
volatile int rxReadIndex = 0;			// Index in buffer to be read
volatile int rxWriteIndex = 0;			// Index in buffer to be written to
volatile uint8_t overflowCnt = 0;		// Number of overflows
volatile uint8_t rxBuffer[RXBUFSIZE];	// Buffer to store data

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

/* UART/LEUART IRQ Handler*/
void USART0_RX_IRQHandler(void) {
	if (USART0->STATUS & USART_STATUS_RXDATAV) {
		/* Store Data */
		rxBuffer[rxWriteIndex] = USART_Rx(USART0);
		rxWriteIndex++;
		if (rxWriteIndex == RXBUFSIZE) {
			rxWriteIndex = 0;
		}
		/* Check for overflow - flush buffer */
		if (rxWriteIndex == rxReadIndex) {
			overflowCnt++;
		}
	}
}

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

void serialInit(void) {

	// $[USART_InitAsync]
	USART_InitAsync_TypeDef initasync = USART_INITASYNC_DEFAULT;

	initasync.enable               = usartDisable;
	initasync.baudrate             = 2000000;
	initasync.databits             = usartDatabits8;
	initasync.parity               = usartNoParity;
	initasync.stopbits             = usartStopbits1;
	initasync.oversampling         = usartOVS16;
	#if defined( USART_INPUT_RXPRS ) && defined( USART_CTRL_MVDIS )
	initasync.mvdis                = 0;
	initasync.prsRxEnable          = 0;
	initasync.prsRxCh              = 0;
	#endif

	USART_InitAsync(USART0, &initasync);
	// [USART_InitAsync]$

	// $[USART_InitSync]
	// [USART_InitSync]$

	// $[USART_InitPrsTrigger]
	USART_PrsTriggerInit_TypeDef initprs = USART_INITPRSTRIGGER_DEFAULT;

	initprs.rxTriggerEnable        = 0;
	initprs.txTriggerEnable        = 0;
	initprs.prsTriggerChannel      = usartPrsTriggerCh0;

	USART_InitPrsTrigger(USART0, &initprs);
	// [USART_InitPrsTrigger]$

	// $[USART_InitIO]
	/* Disable CLK pin */
	USART0->ROUTELOC0	 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_CLKLOC_MASK)) | USART_ROUTELOC0_CLKLOC_LOC0;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN & (~USART_ROUTEPEN_CLKPEN);

	/* Disable CS pin */
	USART0->ROUTELOC0	 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_CSLOC_MASK)) | USART_ROUTELOC0_CSLOC_LOC0;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN & (~USART_ROUTEPEN_CSPEN);

	/* Disable CTS pin */
	USART0->ROUTELOC1	 = (USART0->ROUTELOC1 & (~_USART_ROUTELOC1_CTSLOC_MASK)) | USART_ROUTELOC1_CTSLOC_LOC0;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN & (~USART_ROUTEPEN_CTSPEN);

	/* Disable RTS pin */
	USART0->ROUTELOC1	 = (USART0->ROUTELOC1 & (~_USART_ROUTELOC1_RTSLOC_MASK)) | USART_ROUTELOC1_RTSLOC_LOC0;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN & (~USART_ROUTEPEN_RTSPEN);

	/* Set up RX pin */
	USART0->ROUTELOC0	 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_RXLOC_MASK)) | USART_ROUTELOC0_RXLOC_LOC11;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN | USART_ROUTEPEN_RXPEN;

	/* Set up TX pin */
	USART0->ROUTELOC0	 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_TXLOC_MASK)) | USART_ROUTELOC0_TXLOC_LOC11;
	USART0->ROUTEPEN	  = USART0->ROUTEPEN | USART_ROUTEPEN_TXPEN;

	// [USART_InitIO]$

	// $[USART_Misc]
	/* Disable CTS */
	USART0->CTRLX	  = USART0->CTRLX & (~USART_CTRLX_CTSEN);
	/* Set CTS active low */
	USART0->CTRLX	  = USART0->CTRLX & (~USART_CTRLX_CTSINV);
	/* Set RTS active low */
	USART0->CTRLX	  = USART0->CTRLX & (~USART_CTRLX_RTSINV);
	/* Set CS active low */
	USART0->CTRL	  = USART0->CTRL & (~USART_CTRL_CSINV);
	/* Set TX active high */
	USART0->CTRL	  = USART0->CTRL & (~USART_CTRL_TXINV);
	/* Set RX active high */
	USART0->CTRL	  = USART0->CTRL & (~USART_CTRL_RXINV);
	// [USART_Misc]$

	// $[USART_Enable]

	/* Enable USART if opted by user */
	USART_Enable(USART0, usartEnable);
	// [USART_Enable]$

	/* Clear previous RX interrupts */
	USART_IntClear(USART0, USART_IF_RXDATAV);
	NVIC_ClearPendingIRQ(USART0_RX_IRQn);

	/* Enable RX interrupts */
	USART_IntEnable(USART0, USART_IF_RXDATAV);
	NVIC_EnableIRQ(USART0_RX_IRQn);
}

uint8_t serialAvailable(void) {
	if (rxWriteIndex != rxReadIndex) {
		return 1;
	} else {
		return 0;
	}
}

uint8_t serialCheckError(void) {
	return overflowCnt;
}

void serialFlush(void) {
	rxReadIndex = rxWriteIndex;
	overflowCnt = 0;
}

int serialRead(void) {
	// If the buffer's start is the buffer's end, there's no data (return -1)
	if (rxWriteIndex == rxReadIndex) {
		return -1;
	}
	// Save the first byte to a temporary variable, move the start-pointer
	int r = (int) rxBuffer[rxReadIndex++];
	if (rxReadIndex == RXBUFSIZE) {
		rxReadIndex = 0;
	}
	// and return the stored byte.
	return r;
}

void serialWrite(uint8_t data) {
	USART_Tx(USART0, data);
}

void serialPrint(char * data) {
	int cnt = 0;
	while (data[cnt] != 0x00) {
		USART_Tx(USART0, data[cnt]);
		cnt++;
	}
}

void serialPrintInt(uint32_t number) {

	char charBuf[20];
	sprintf(charBuf, "%d", (int) number);
	serialPrint(charBuf);

}

void serialPrintLn(char * data) {
	serialPrint(data);
	serialPrint("\r\n");
}

