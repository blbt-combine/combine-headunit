/*******************************************************************************
 * FPGA FLASHER LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 11.04.2017
 * Description: A basic library to flash an iCE40 device using SPI.
 *      The library assumes that USART module as well
 *      as the I/Os have successfully been configured.
 *
 *****************************************************************************/

#include "fpga-flasher.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/
void simpleDelay(int delayMS);

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/
void simpleDelay(int delayMS) {
	long tmp = 0;
	for (tmp = 0; tmp < delayMS * 40000; tmp++)
		;
}

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/
void fpga_reset(void) {
	/* Keep FPGA in reset state: */
	GPIO_PinOutClear(FPGA_CRESET_PORT, FPGA_CRESET_PIN);
}

void fpga_flash(const unsigned char * binary, unsigned int binary_len) {

	/*
	 * set CRESET high for starting the sequence
	 * while keeping CS low
	 * see iCE40 Programming and Configuration, page 4
	 */
	GPIO_PinOutSet(FPGA_CS_PORT, FPGA_CS_PIN);
	GPIO_PinOutSet(FPGA_CRESET_PORT, FPGA_CRESET_PIN);
	simpleDelay(10);
	GPIO_PinOutClear(FPGA_CS_PORT, FPGA_CS_PIN);
	GPIO_PinOutClear(FPGA_CRESET_PORT, FPGA_CRESET_PIN);
	simpleDelay(1);
	GPIO_PinOutSet(FPGA_CRESET_PORT, FPGA_CRESET_PIN);
	simpleDelay(2);

	/* write the data using SPI */
	long cnt = 0;
	for(cnt = 0; cnt < binary_len; cnt++) {
		USART_SpiTransfer(defaultSPI, binary[cnt]);
	}

	/* check for CDONE to go high or wait */
	//while(!GPIO_PinInGet(FPGA_CDONE_PORT,FPGA_CDONE_PIN));
	simpleDelay(10);

	/*
	 * clock at least for another 49 times to release all GPIO
	 * 7 * 8 bits = 56 clocks
	 */
	for(cnt = 0; cnt < 8; cnt++) {
		USART_SpiTransfer(defaultSPI, 0x00);
	}

	/*
	 * cycle reset to boot the FPGA
	 */
	GPIO_PinOutSet(FPGA_CS_PORT, FPGA_CS_PIN);

}
