/*******************************************************************************
 * RADIO LIBRARY
 *******************************************************************************
 *
 * Author: Sebastian Stoecklin
 * Date: 03.07.2017
 * Description: A simple library for accessing the RAIL library
 *
 ******************************************************************************/

#include "radio.h"
#include "serial.h"
#include "pa.h"

#include "rail_config.h"
#include "hal_common.h"


/******************************************************************************
 * VARIABLES
 *****************************************************************************/

volatile int railBufferRx[MAX_BUFFER_SIZE];
volatile uint8_t railBufferTx[MAX_BUFFER_SIZE];
volatile int channel = 0;
volatile int currentConfig = 0; //default is first in list

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

/******************************************************************************
 * RAIL Callback Implementation
 *****************************************************************************/
void RAILCb_TxPacketSent(RAIL_TxPacketInfo_t *txPacketInfo) {
#if DEBUG_PRINT_RAIL
	serialPrintLn("TX successful.");
#endif
	packetRx = true;
}

void RAILCb_RxPacketReceived(void *rxPacketHandle) {

	RAIL_RxPacketInfo_t *rxPacketInfo = (RAIL_RxPacketInfo_t*) rxPacketHandle;
	uint8_t * data = rxPacketInfo->dataPtr;

	rxBufferLength = rxPacketInfo->dataLength;
	uint16_t i;
	for (i = 0; i < rxBufferLength; i++) {
		rxBuffer[i] = data[i];
	}

	packetRx = true;
}

void RAILCb_RxRadioStatus(uint8_t status) {
	if (status & RAIL_RX_CONFIG_INVALID_CRC) {
#if DEBUG_PRINT_RAIL
		serialPrintLn("Invalid CRC.");
#endif
	}
	if (status & RAIL_RX_CONFIG_SYNC1_DETECT) {
#if DEBUG_PRINT_RAIL
		serialPrintLn("Sync detect.");
#endif
	}
	if (status & RAIL_RX_CONFIG_PREAMBLE_DETECT) {
#if DEBUG_PRINT_RAIL
		serialPrintLn("Preamble detect.");
#endif
	}
	if (status & RAIL_RX_CONFIG_BUFFER_OVERFLOW) {
#if DEBUG_PRINT_RAIL
		serialPrintLn("RX Buffer overflow.");
#endif
	} else if (status & RAIL_RX_CONFIG_ADDRESS_FILTERED) {
#if DEBUG_PRINT_RAIL
		serialPrintLn("Address filtered.");
#endif
	}
}

void RAILCb_TxRadioStatus(uint8_t status) {
	if (status & RAIL_TX_CONFIG_BUFFER_UNDERFLOW) {
#if DEBUG_PRINT_RAIL
		serialPrintLn("TX Buffer Underflow.");
#endif
		packetRx = true;
	}
	if (status & RAIL_TX_CONFIG_CHANNEL_BUSY) {
#if DEBUG_PRINT_RAIL
		serialPrintLn("Channel busy.");
#endif
	}
	if (status & RAIL_TX_CONFIG_TX_ABORTED) {
#if DEBUG_PRINT_RAIL
		serialPrintLn("TX aborted.");
#endif
	}
	if (status & RAIL_TX_CONFIG_TX_BLOCKED) {
#if DEBUG_PRINT_RAIL
		serialPrintLn("TX blocked.");
#endif
	}
}

void *RAILCb_AllocateMemory(uint32_t size) {
	return railBufferRx;
}

void *RAILCb_BeginWriteMemory(void *handle, uint32_t offset,
		uint32_t *available) {
	return ((uint8_t*) handle) + offset;
}

void RAILCb_EndWriteMemory(void *handle, uint32_t offset, uint32_t size) {
	// Do nothing
}

void RAILCb_FreeMemory(void *ptr) {
}

void RAILCb_RadioStateChanged(uint8_t state) {
}

void RAILCb_RfReady() {
}

void RAILCb_TimerExpired() {
}

void RAILCb_CalNeeded() {
}

/******************************************************************************
 * LIBRARY IMPLEMENTATION
 *****************************************************************************/

void initRadio(void) {

	/* enable PA */
	RADIO_PAInit_t paInit;
	paInit = (RADIO_PAInit_t) RADIO_PA_2P4_INIT;

	if (!RADIO_PA_Init(&paInit)) {
		// Error: The PA could not be initialized due to an improper configuration.
		// Please ensure your configuration is valid for the selected part.
		while (1)
			;
	}

	RAIL_Init_t railInitParams = { 256,
	RADIO_CONFIG_XTAL_FREQUENCY,
	RAIL_CAL_ALL, };
	RAIL_RfInit(&railInitParams);
	RAIL_RfIdleExt(RAIL_IDLE, true);

	RAIL_CalInit_t calInit = {
	RAIL_CAL_ALL, irCalConfig, };
	RAIL_CalInit(&calInit);

	RAIL_PacketLengthConfigFrameType(frameTypeConfigList[0]);
	if (RAIL_RadioConfig((void*) configList[0])) {
		while (1)
			;
	}

	RAIL_ChannelConfig(channelConfigs[0]);

	// Configure RAIL callbacks with no appended info
	RAIL_RxConfig(
			( RAIL_RX_CONFIG_INVALID_CRC | RAIL_RX_CONFIG_SYNC1_DETECT
					| RAIL_RX_CONFIG_ADDRESS_FILTERED
					| RAIL_RX_CONFIG_BUFFER_OVERFLOW),
			false);
}

void radioTx(uint8_t * data, uint16_t length) {
	// Build packet for variable packet size:
	railBufferTx[0] = (uint8_t) length;
	uint16_t i;
	for (i = 0; i < length; i++) {
		railBufferTx[1 + i] = data[i];
	}

	RAIL_TxData_t payload = { (uint8_t *) &railBufferTx[0], length + 1 };
	RAIL_RfIdleExt(RAIL_IDLE, true);
	RAIL_TxDataLoad(&payload);
	RAIL_TxStart(channel, NULL, NULL);
}

void radioStartRx(void) {
	RAIL_RfIdleExt(RAIL_IDLE, true);
	RAIL_RxStart(channel);
}
