/* EFR32 specific includes */
#include <binary-header.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_usart.h"
#include "em_timer.h"
#include "em_vdac.h"

/* Hardware configuration functions */
#include "InitDevice.h"

/* Own libraries */
#include "serial.h"
#include "fpga-flasher.h"
#include "peripherals.h"
#include "timer.h"
#include "gpiointerrupt.h"
#include "radio.h"
#include "adc.h"

/* Own headers */
#include "printMessages.h"
#include "misc.h"
#include "config.h"

/* Radio variables */
volatile uint8_t txBuffer[APP_MAX_PACKET_LENGTH] = { 0 };
volatile uint16_t txBufferLength = 0;

volatile uint8_t rxBuffer[APP_MAX_PACKET_LENGTH] = { 0 };
volatile uint16_t rxBufferLength = 0;

volatile bool packetTx = false;
volatile bool packetRx = true;

/* timer variables */
volatile uint32_t tick = 0;
void (*timerCallback)(void);

/* utility prototypes */
void initController(void);
void periodicFunction(void);

/* app prototypes */
void app_echo(void);
void app_rfTerminal(void);
void app_test(void);

/* app variables */
volatile uint8_t buttonPressed = 0;
volatile uint8_t caps = BIT1;

uint32_t sampleCount = 0;
uint32_t currentCoilRf = 0;
uint32_t currentAmpDC = 0;
uint32_t voltageAmpDC = 3000;
bool newAdcValue = false;
bool measurementActive = false;

/******************************************************************************
 * MAIN
 *****************************************************************************/

int main(void) {

	timerCallback = periodicFunction;

	initController();
	initRadio();

	/* Infinite loop */
	while (1) {
		app_echo();
	}

	return 0;
}

/******************************************************************************
 * UTILITIES AND CALLBACKS
 *****************************************************************************/

void initController(void) {
	/* start chip */
	enter_DefaultMode_from_RESET();

	/* init internal modules */
	timerInit(TICK_TIME_US, timerCallback);
	serialInit();
	//adcInit();
	dac_enable();

	/* set configurable supply voltages (3.3 V reference) */
	VDAC_ChannelOutputSet(VDAC0, 0, 1540); // VDDCORE_FPGA to 1.2 V
	VDAC_ChannelOutputSet(VDAC0, 1, 2350); // VDDIO_FPGA to 1.8 V

	/* configure peripherals */

	/* enable FPGA connected oscillator */
	//osc_enable();
	osc_disable();
	/* boot FPGA */
	fpga_flash(readoutController_bitmap_bin, readoutController_bitmap_bin_len);

}

void periodicFunction(void) {
	//serialWrite('A');
}


/******************************************************************************
 * APPS
 *****************************************************************************/

void app_echo(void) {
	/* Echo characters: */
	if (serialAvailable()) {
		serialWrite((uint8_t) serialRead());
	}
}

void app_rfTerminal(void) {
	/* RF Interface */
	/* Put characters in a buffer to transmit via radio: */
	while(serialAvailable()) {
		uint8_t tmp = serialRead();
		txBuffer[txBufferLength++] = tmp;
		txBufferLength %= APP_MAX_PACKET_LENGTH;
		packetTx = true;
		wait_ms(10);
	}
	/* Print received packages to terminal */
	if (rxBufferLength > 0) {
		uint16_t i;
		// dont't print the first one, as it is the number of transmitted bytes:
		for (i = 1; i < rxBufferLength; i++) {
			serialWrite(rxBuffer[i]);
		}
		rxBufferLength = 0;
	}

	if (packetTx) {
		// Data ready to be transmitted:
		packetTx = false;
		radioTx(txBuffer, txBufferLength);
		wait_ms(100);
		txBufferLength = 0;
	}

	if (packetRx) {
		// Packet received:
		packetRx = false;
		radioStartRx();
	}
}

void app_test(void) {
	// Write your test app here...
}

