// This file is generated by Simplicity Studio.  Please do not edit manually.
//
//

#include PLATFORM_HEADER
#include CONFIGURATION_HEADER
#include <stdint.h>
#include "rail.h"
#include "rail_types.h"
#include "rail_ieee802154.h"


/**
 * Callback that notifies the application when searching for an ACK has timed
 * out.
 *
 * @return void
 *
 * This callback function is called whenever the timeout for searching for an
 * ack is exceeded.
 */
void RAILCb_RxAckTimeout(void)
{
}


/**
 * Callback for when a Data Request is being received
 *
 * @param address The source address of the data request command
 *
 * This function is called when the command byte of an incoming frame is for a
 * data request, which requests an ACK. This callback will be called before the
 * packet is fully received, to allow the node to have more time to decide
 * whether to set frame pending in the outgoing ACK.
 */
void RAILCb_IEEE802154_DataRequestCommand(RAIL_IEEE802154_Address_t *address)
{
}


/**
 * Callback for when AGC averaged RSSI is done
 *
 * Called in response to RAIL_StartAverageRSSI.
 */
void RAILCb_RssiAverageDone(int16_t avgRssi)
{
}


/**
 * Callback that fires when the Rx Fifo exceeds the configured threshold value
 *
 * @param[in] bytesAvailable Number of bytes available in the Rx Fifo at the
 * time of the callback dispatch
 *
 * @return void
 * @warning You must implement a stub for this in your RAIL application.
 *
 * Callback that fires when the Rx Fifo exceeds the configured threshold value.
 * Provides the number of bytes available in the Rx Fifo at the time of the
 * callback dispatch.
 */
void RAILCb_RxFifoAlmostFull(uint16_t bytesAvailable)
{
}


/**
 * Callback that fires when the Tx Fifo falls under the configured threshold
 * value
 *
 * @param[in] spaceAvailable Number of bytes open in the Tx Fifo at the time of
 * the callback dispatch
 *
 * @return void
 * @warning You must implement a stub for this in your RAIL application.
 *
 * Callback that fires when the Tx Fifo falls under the configured threshold
 * value. It only fires if a rising edge occurs across this threshold. This
 * callback will not fire on initailization nor after resetting the transmit
 * fifo with RAIL_ResetFifo().
 *
 * Provides the number of bytes open in the Tx Fifo at the time of the callback
 * dispatch.
 */
void RAILCb_TxFifoAlmostEmpty(uint16_t spaceAvailable)
{
}


